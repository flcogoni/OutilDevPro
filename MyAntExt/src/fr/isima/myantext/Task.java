package fr.isima.myantext;

public abstract class Task {

	public abstract void execute();
}
