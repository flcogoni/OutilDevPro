package fr.isima.myantext;
import java.util.Vector;

public class Project {
	String name;
	Target defaultTarget;
	Vector <Target> targets;
	
	public Project() {
		targets = new Vector <Target>();
	}
	
	public Target getDefaultTarget() {
		return defaultTarget;
	}

	public void setDefaultTarget(Target defaultTarget) {
		this.defaultTarget = defaultTarget;
	}
	
	public Vector<Target> getTargets() {
		return targets;
	}
	
	public void addTarget(Target t) {
		if(targets.isEmpty())
			targets.add(t);
		else if (!targets.contains(t))
			targets.add(t);
	}

	public void setTargets(Vector <Target> targets) {
		this.targets = targets;
	}

	public static void loadFromFile() {
		
	}
	
	public boolean AreDependanciesExecuted(Vector <Target> dep ) {
		int  i=0;
		while(i<dep.size() && dep.elementAt(i).getIsExecuted())
			i++;
		return (dep.size()==0 || i==dep.size());
	}
	
	public void execute(Target t) {
		if(!t.getIsExecuted() && AreDependanciesExecuted(t.getDependancies())) {
			t.execute();
		}
		else {
			if(!t.getIsExecuted()){
				for(Target dep : t.getDependancies())
					execute(dep);
				execute(t);
				//t.rmDependancies(0);
			}
		}
	}
	
	public void execute() {
		if(targets.isEmpty())
			defaultTarget.execute();
		else {
			for(Target t : targets) {
				execute(t);
			}
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Recherche une target par son nom dans le vecteur de target du projet
	 * @param targets
	 * @param dependance
	 * @return la target qui a le nom donn� en param�tre
	 */
	public Target findTarget(String dependance) {
		int i=0;
		while(i<targets.size() && !(targets.get(i).getName()).equals(dependance)) {
			i++;
		}
		if(i<targets.size())
			return targets.get(i);
		else
			return null;
	}
	
	@Override
	public String toString() {
		String result ="";
		for(Target t : targets) {
			result += t.getName() +" : ";
			for(Target d : t.getDependancies()) {
				if(d!=null)
					result += d.getName() + ", ";
			}
			result +="\n";
				
		}
		result += defaultTarget.getName() +" : ";
		for(Target d : defaultTarget.getDependancies())
			result += d.getName() + ", ";
;		return result;
	}

}
