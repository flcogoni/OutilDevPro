package fr.isima.myantext;

import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Vector;


/**
 * Classe pour stocker les objets trait�s (targets)
 * @author Mathilde
 *
 */
class MyObject{
	String name;
	Class var;
	
	MyObject(String s, Class c){
		name = s;
		var = c;
	}
	String getString() {
		return name;
	}
	Class getVar() {
		return var;
	}
	void setVar(Class v) {
		var = v;
	}
}

/**
 * g�n�re le chemin absolu du projet
 * @author Mathilde
 *
 */
public class MainClass {
	static String path;
	
	public MainClass() {
		File file = new File("CopyTask.java");
		path = file.getAbsolutePath();
		path = path.split("CopyTask.java")[0];
		path +="src\\fr\\isima\\myantext\\";
	}
	
	/**
	 * Cherche dans un vecteur de MyObject l'�l�ment ayant le nom donn�
	 * @param MyObjects
	 * @param name 
	 * @return le MyObject ayant le nom donn� en param�tre
	 */
	public static MyObject find(Vector <MyObject> MyObjects, String name) {
		int i=0;
		while(i<MyObjects.size() && !(MyObjects.get(i).getString()).equals(name)) {
			i++;
		}
		return MyObjects.get(i);
	}
	
	public static void main(String[] args) {
		new MainClass();
		
		try {
			
			/**
			 * Cr�ation du projet
			 */
			Project myProject = new Project();
			myProject.setName("Mon Super Projet");
	
			String ligne;
			Target MyTarget;
			BufferedReader in = new BufferedReader(new FileReader(path + "build.txt"));	//Chargement du ficher build.txt
			Vector <MyObject> MyObjects = new Vector<MyObject>();					//Vecteur contenant tous les objets � charger
			Vector <String []> Dependances = new Vector<String[]>();				//Vecteur contenant la liste des d�pendances � g�rer
		
			ligne = in.readLine();
			while(ligne!=null) {
				if(!ligne.startsWith("#") && ligne.length()!=0) {
					
					/**
					 * Lecture des "use"
					 */
					if(ligne.startsWith("use")) {
						String name[];
						name = ligne.split(" ");
						MyObjects.add(new MyObject(name[3],Class.forName(name[1])));	//Cr�ation des targets qui seront utilis�es
						
						ligne= in.readLine();
						
					/**
					 * Lecture des targets
					 */
					}else if(ligne.contains(":")) {
						MyTarget = new Target();
						String dep[] = ligne.split(":");
						MyTarget.setName(dep[0]);			//Nom de la target
						if(dep.length >=2) {					//Lecture des d�pendances
							dep[1] = dep[1].replaceAll(" ", "");
							dep = dep[1].split(",");
							
							Dependances.add(dep);
						}
						else
							Dependances.add(null);			//Pas de d�pendances
						
						/**
						 * Lecture de la default Task
						 */
						if (ligne.startsWith("default:")) {	
							ligne= in.readLine();
							do {
								ligne = ligne.split("\\]")[0];
								String methode[] = ligne.split("\\[");		
								String tmp[] = {methode[1]};					//Arguments
								String arguments[] = ((methode[1].contains(","))?methode[1].split(","):tmp);	//S�paration des arguments
								Object [] parameters = new Object[arguments.length];
								Class [] parametersType = new Class[arguments.length];
								
								for(int j=0; j<arguments.length;j++) {
									String value = arguments[j].split("\"")[1];
									Class c = arguments[j].getClass();
									Constructor cons = String.class.getConstructor(c);
									parameters[j] = value;						//Argument
									parametersType[j] = c;						//Type de l'argument
								}
								String name_cons = methode[0];					//Nom de la methode
								name_cons = Character.toUpperCase(name_cons.charAt(0)) + name_cons.substring(1)+"Task";
								MyObject o = find(MyObjects, methode[0]);
								Constructor consTask= o.getVar().getConstructor(parametersType);
								
								Task t = (Task) consTask.newInstance(parameters);
								MyTarget.addTask(t);							//Ajout de la task
							}while((ligne= in.readLine()) !=null && !(ligne.contains(": ") || ligne.indexOf(':')==ligne.length()-1));
							myProject.setDefaultTarget(MyTarget);				//Ajout de la default target
							
						}else{

							/**
							 * Lecture des autres Targets
							 */
							ligne= in.readLine();
							do {								
								ligne = ligne.split("\\]")[0];
								String methode[] = ligne.split("\\[");
								String tmp[] = {methode[1]};					//Arguments
								String arguments[] = ((methode[1].contains(","))?methode[1].split(","):tmp);	//S�paration des arguments
								Object [] parameters = new Object[arguments.length];
								Class [] parametersType = new Class[arguments.length];
								for(int j=0; j<arguments.length;j++) {
									String value = arguments[j].split("\"")[1];
									Class c = arguments[j].getClass();
									Constructor cons = String.class.getConstructor(c);
									parameters[j] = value;						//Argument
									parametersType[j] = c;						//Type de l'argument
								}						
								String name_cons = methode[0];					//Nom de la methode
								name_cons = Character.toUpperCase(name_cons.charAt(0)) + name_cons.substring(1)+"Task";
								MyObject o = find(MyObjects, methode[0]);
								Constructor consTask= o.getVar().getConstructor(parametersType);
								
								Task t = (Task) consTask.newInstance(parameters);
								MyTarget.addTask(t);							//Ajout de la task
								
							}while((ligne= in.readLine()) !=null && !(ligne.contains(": ") || ligne.indexOf(':')==ligne.length()-1));
							
							myProject.addTarget(MyTarget);						//Ajout de la target
						}
					}
				}else
					ligne= in.readLine();
			}
			/**
			 * Ajout des d�pendances
			 */
			for(int i=1; i<Dependances.size();i++) {
				if(Dependances.elementAt(i) !=null) {
					for(String dependances : Dependances.elementAt(i)) {
						if(dependances != null)
							myProject.getTargets().elementAt(i-1).addDependancies(myProject.findTarget(dependances));
					}					
				}				
			}
			
			myProject.execute();		//Lancement du projet
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
