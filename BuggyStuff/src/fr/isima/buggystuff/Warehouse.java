package fr.isima.buggystuff;

import java.util.ArrayList;
import java.util.List;

public class Warehouse {
	
	protected int id;
	protected String name;
	protected List<Entry> entries;
	
	public Warehouse(int id, String name) {
		super();
		this.id = id;
		this.name = name;
		entries = new ArrayList<Entry>();
	}
	
	public void addProduct(Product product) {
		Entry entry = new Entry(this, product);
		entries.add(entry);
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}


	public Product getProduct(int id) {
		for(Entry entry: entries) {
			if(entry.getProduct().getId() == id) {
				return entry.getProduct();
			}
		}
		return new Product(id,"not found");
	}

	public Product getProduct(String name) {
		if(name!=null) {
			int i = 0;
			for(; i <= entries.size(); i++) {
				Entry entry = entries.get(i);
				if(name.equals(entry.getProduct().getName())) {
					return entry.getProduct();
				}
			}
		}
		return new Product(-1,name);
	}

}
