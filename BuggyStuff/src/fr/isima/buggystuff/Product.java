package fr.isima.buggystuff;

public class Product {

	protected int id;
	protected String name;
	
	public Product(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public String toString() {
		if(id == -1 || name =="not found")
			return "Product [id=" + id + ", name=" + name + "] not found";
		else
			return "Product [id=" + id + ", name=" + name + "]";
			
	}
	
}
