package fr.isima.buggystuff;

public class Entry {
	
	protected Warehouse warehouse;
	protected Product product;
	
	public Entry(Warehouse warehouse, Product product) {
		super();
		this.warehouse = warehouse;
		this.product = product;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public Product getProduct() {
		return product;
	}

}
