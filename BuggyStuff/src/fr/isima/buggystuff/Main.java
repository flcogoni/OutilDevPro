/**
 * Classe principale du projet, utilisée pour lancer quelques tests.
 */
package fr.isima.buggystuff;

public class Main {
	

	public static void main(String[] args) {
		Warehouse warehouse = new Warehouse(100, "Clermont-Ferrand");
		for(int i = 0; i < 100; i++) {
			Product product = new Product(100+i, "Product#"+i);
			warehouse.addProduct(product);
		}
		
		
		System.out.println(warehouse.getProduct(0).toString());
		System.out.println(warehouse.getProduct(50).toString());
		System.out.println(warehouse.getProduct(100).toString());

		System.out.println(warehouse.getProduct(null).toString());
		System.out.println(warehouse.getProduct("Product#10").toString());
		System.out.println(warehouse.getProduct("Product#20").toString());
	}

}
