package fr.isima.myant;

import java.io.File;

public class MkdirTask extends Task{
	String dir;

	public MkdirTask(String d) {
		dir= d;
	}
	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}
	
	public void execute() {
		File f = new File (dir);
		f.mkdirs();
	}
}
