package fr.isima.myant;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class CopyTask extends Task{
	String from;
	String to;
	
	public CopyTask(String f, String t) {
		from = f;
		to = t;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	
	public void execute() {
		File input = new File(from);
		File output = new File(to);
		try {
			if(output.exists()) {
				output.delete();
			}				
			Files.copy(input.toPath(), output.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
