package fr.isima.myant;

public abstract class Task {

	public abstract void execute();
}
