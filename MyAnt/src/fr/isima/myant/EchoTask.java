package fr.isima.myant;

public class EchoTask extends Task{
	String message;
	
	public EchoTask(String m) {
		message = m;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public void execute() {
		System.out.println(message);
	}
}
