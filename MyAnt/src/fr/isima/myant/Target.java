package fr.isima.myant;

import java.util.Vector;

public class Target {
	String name;
	Vector <Target> dependancies;
	Vector <Task> tasks;
	boolean isExecuted;
	
	public Target() {
		isExecuted = false;
		dependancies = new Vector <Target>();
		tasks = new Vector <Task>();
	}
	
	public void setIsExecuted(boolean b) {
		isExecuted = b;
	}
	public boolean getIsExecuted() {
		return isExecuted;
	}
	public Vector <Task> getTasks() {
		return tasks;
	}

	public void setTasks(Vector <Task> tasks) {
		this.tasks = tasks;
	}

	public Vector<Target> getDependancies() {
		return dependancies;
	}

	public void setDependancies(Vector <Target> dependancies) {
		this.dependancies = dependancies;
	}

	public void addDependancies(Target t) {
		dependancies.add(t);
	}
	
	public void rmDependancies(int i) {
		dependancies.remove(i);
	}
	
	public void addTask(Task t) {
		tasks.add(t);
	}
	
	public void execute() {
		for(Task t : tasks) {
			t.execute();
		}
		isExecuted = true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
