import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.isima.myant.CopyTask;
import fr.isima.myant.EchoTask;
import fr.isima.myant.MkdirTask;
import fr.isima.myant.Project;

public class TestTaskMyAnt {
	String path;
	
	public TestTaskMyAnt() {
		File file = new File("TestTaskMyAnt.java");
		path = file.getAbsolutePath();
		path = path.split("TestTaskMyAnt.java")[0];
		path += "src\\";
	}
	@Test
	public void AloneDefaultTarget() {
		Project p = new Project(path + "Build_OnlyDefaultTarget.txt");
		
		assertTrue(p.getTargets().isEmpty());
		assertEquals(p.getDefaultTarget().getName(),"default");
		assertEquals(p.getDefaultTarget().getTasks().size(),2);
	}
	
	@Test
	public void DefaultAndOneTarget() {
		Project p = new Project(path + "Build_DefaultAndOneTarget.txt");
		
		assertEquals(p.getTargets().size(),1);
		
		assertEquals(p.getDefaultTarget().getName(),"default");
		assertEquals(p.getDefaultTarget().getTasks().size(),2);
		
		assertTrue(p.getTargets().elementAt(0).getIsExecuted() && !p.getDefaultTarget().getIsExecuted());
	}
	
	@Test
	public void CheckDependancies() {
		Project p = new Project(path + "Build_CheckDependancies.txt");
		
		assertEquals(p.getTargets().elementAt(0).getDependancies().elementAt(0),p.getTargets().elementAt(2));
		assertEquals(p.getTargets().elementAt(0).getDependancies().elementAt(1),p.getTargets().elementAt(1));
		
		assertTrue(p.getTargets().elementAt(1).getDependancies().isEmpty());
		
		assertEquals(p.getTargets().elementAt(2).getDependancies().elementAt(0),p.getTargets().elementAt(1));
	}
	@Test
	public void EchoTest() {
		Project p = new Project(path + "Build_EchoTest.txt");
		
		assertEquals(EchoTask.class, p.getDefaultTarget().getTasks().elementAt(0).getClass());
		assertEquals(((EchoTask)p.getDefaultTarget().getTasks().elementAt(0)).getMessage(),"message");
		
	}
	@Test
	public void MkdirTest() {
		Project p = new Project(path + "Build_MkdirTest.txt");
		
		assertEquals(MkdirTask.class, p.getDefaultTarget().getTasks().elementAt(0).getClass());
		assertEquals(((MkdirTask)p.getDefaultTarget().getTasks().elementAt(0)).getDir(),path+ "MonDossier");
		
		File f = new File(path + "MonDossier");
		assertTrue(f.exists() && f.isDirectory());
		
	}
	@Test
	public void CopyTest() {
		Project p = new Project(path + "Build_CopyTest.txt");
		
		assertEquals(CopyTask.class, p.getDefaultTarget().getTasks().elementAt(0).getClass());
		assertEquals(((CopyTask)p.getDefaultTarget().getTasks().elementAt(0)).getFrom(),path+"MonTexte");
		assertEquals(((CopyTask)p.getDefaultTarget().getTasks().elementAt(0)).getTo(),path + "MonTexteBis");
		
		File f2 = new File(path + "MonTexteBis");
		assertTrue(f2.exists() && f2.isFile());
		
		File f1 = new File(path + "MonTexte");
		FileReader fR1=null, fR2=null; 
		try {
			fR1 = new FileReader(f1);
			fR2 = new FileReader(f2);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        BufferedReader reader1 = new BufferedReader(fR1);
        BufferedReader reader2 = new BufferedReader(fR2);

        String line1 = null;
        String line2 = null;
        try {
	        while (((line1 = reader1.readLine()) != null)  && ((line2 = reader2.readLine()) != null)) {
	        	assertEquals(line1,line2);
	        }
        
			reader1.close();
			reader2.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		
		
		
	}
}
