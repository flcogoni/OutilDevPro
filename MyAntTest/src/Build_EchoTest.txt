# The imports section lists the external tasks to use
# The external tasks must be in the classpath, but NOT in the build path
use fr.isima.myant.EchoTask as echo

# For the moment, the default target acts as the entry point of your project
# No other targets will be defined
default:
echo[message:"message"]